/**
 * View Models used by Spring MVC REST controllers.
 */
package com.orangesonatel.scorpion.web.rest.vm;
